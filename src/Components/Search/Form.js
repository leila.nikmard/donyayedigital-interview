import React from "react";

function Form ({data , handleChange , handleSubmit}){

    return(
        <form>
            <input 
                type="text" 
                value={data} 
                onChange={handleChange} 
                placeholder="Search"
            />
            <button 
                type="submit" 
                onClick={handleSubmit}
            >
                submit
            </button>
        </form>
    )

}

export default Form;