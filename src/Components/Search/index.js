import React, { useCallback, useState } from "react";
import Loading from "../../Components/Loading/index";
import { getSearchDataService } from "../../utils/services/index";
import Form from "./Form";
import "./style.css"

function Search() {
    const [inputData, setInputData] = useState("");
    const [searchData, setSearchData] = useState([])
    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = useCallback((e) => {
        e.preventDefault();
        setIsLoading(true);
        getSearchDataService(inputData)
            .then(res => res.json())
            .then(response => {
                if (response.meta.status) {
                    setIsLoading(false);
                    const data = response.data;
                    setSearchData(data);
                }
            })
    }, [inputData]);

    const handleChange = (e) => {
        setInputData(e.target.value)
    };

    return (
        <React.Fragment>
            <h1>Search:</h1>
            <Form
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                data={inputData}
            />
            <div className="container-search">
                {
                    searchData.length > 0 ?
                        searchData.map(it => (
                            <div className="image-box">
                                <img src={it.images.original.url} width="200px" height="200px" />
                            </div>
                        ))
                        : isLoading ? <Loading isLoading={isLoading} /> : "no data"
                }
            </div>
        </React.Fragment>
    )

}

export default Search;