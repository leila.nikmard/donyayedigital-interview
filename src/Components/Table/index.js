import React from "react";
import NoItem from "../NoItem";
import "./style.css"

function Table ({data , handleDelete , handleEdit}){
    return(
        <table>
            <caption>List</caption>
            <thead>
                <tr>
                    <th>FirstName</th>
                    <th>LastName</th>
                    <th>NationalCode</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {data.length > 0 ?
                    data.map((it , index) => (
                    <tr key={index}>
                        <td>{it.FirstName}</td>
                        <td>{it.LastName}</td>
                        <td>{it.NationalCode}</td>
                        <td>
                            <button className="edit-btn" onClick={() => handleEdit(it.Id)}>Edit</button>
                        </td>
                        <td>
                            <button className="delete-btn" onClick={() => handleDelete(it.Id)}>Delete</button>
                        </td>
                    </tr>
                    ))
                    :
                    <NoItem />
                }
            </tbody>
        </table>
    )
}

export default Table;