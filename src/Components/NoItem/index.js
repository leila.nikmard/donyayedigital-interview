import React from "react";
import "./style.css";

function NoItem(){
    return (
        <tr className="no-item">
            <td colSpan="5">
                <span>No information found</span>
            </td>
        </tr>
    )
}

export default NoItem;