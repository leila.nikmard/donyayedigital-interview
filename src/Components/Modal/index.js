import React from "react";
import {images} from "../../utils/imageSource";
import "./style.css";

function Modal ({data , handleClose}){
    const {close} = images;

    return (
        <div className="container-modal">
            <div className="close-icon" onClick={handleClose}>
                <img src={close} width="20px" height="20px"/>
            </div>
            <div className="modal-box">
                <h2>{data.task}</h2>
                <p>{data.description}</p>
            </div>
        </div>
    )

}

export default Modal;