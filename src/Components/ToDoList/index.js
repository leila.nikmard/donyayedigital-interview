import React, { useState } from "react";
import {data} from "../../utils/toDoListData/toDoListData";
import {images} from "../../utils/imageSource";
import Modal from "../Modal";
import Form from "./Form";
import "./style.css";

function ToDoList (){

    const [toDoList , setToDoList] = useState(data);
    const [ toDoInput, setToDoInput] = useState({
        task:"",
        description:""
    });
    const [showModal , setShowModal] = useState(false);
    const [modalData , setModalData] = useState();

    const {close} = images;

    const handleChange = (e) => {
        const {name , value} = e.target;
        setToDoInput({
            ...toDoInput,
            [name] : value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let copy = [...toDoList];
        copy = [...copy, { id: toDoList.length + 1, task: toDoInput.task , description: toDoInput.description}];
        setToDoList(copy);
        setToDoInput({
            task:"",
            description:""
        });
    }

    const handleDelete = (id) => {
        const deleteItem = toDoList.filter(it => it.id !== id);
        setToDoList(deleteItem)
    }

    const handleshowDetails = (id) => {
        setShowModal(!showModal);
        const detailsData = toDoList.find(it => it.id === id);
        setModalData(detailsData)
    }

    const handleClose = () => {
        setShowModal(!showModal)
    }

    return(
        <React.Fragment>
            <h1>TODO List:</h1>
            <ul className="lists">
                {toDoList.length > 0 &&
                    toDoList.map(it => (
                        <li key={it.id}>
                            <span 
                                onClick={() => handleshowDetails(it.id)}
                            >
                                {it.task}
                            </span>
                            <img src={close} width="15px" height="15px" onClick={() => handleDelete(it.id)}/>
                        </li>
                    ))
                }
            </ul>
            <Form 
                data={toDoInput}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
            />
            {showModal && <Modal data={modalData} handleClose={handleClose}/>}
        </React.Fragment>
    )

}

export default ToDoList;