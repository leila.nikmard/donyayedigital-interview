import React from "react";

function Form ({data , handleSubmit , handleChange}){
    return(
        <form>
            <input 
                type="text" 
                name="task" 
                value={data.task} 
                onChange={handleChange} 
                placeholder="Enter Task"
            />
            <input 
                type="text" 
                name="description" 
                value={data.description} 
                onChange={handleChange} 
                placeholder="Description"
            />
            <button 
                type="submit" 
                onClick={handleSubmit}
            >
                submit
            </button>
        </form>
    )
}

export default Form;