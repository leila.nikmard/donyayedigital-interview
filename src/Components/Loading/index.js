import React from "react";
import "./style.css";

const Loading = ({isLoading}) => {
  return (
    <>
      {isLoading && (
        <p className="loading">
          Loading ...
        </p>
      )}
    </>
  );
};

export default Loading;
