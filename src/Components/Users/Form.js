import React from "react";

function Form ({handleChange , data , handleSubmit , isEdit}){
    return (
        <div>
            <h1>User List:</h1>
            <form>
                <label>
                    FirstName:
                    <input 
                        type="text" 
                        name="FirstName" 
                        onChange={handleChange}
                        value={data.FirstName}
                    />
                </label>
                <label>
                    LastName:
                    <input 
                        type="text" 
                        name="LastName" 
                        onChange={handleChange} 
                        value={data.LastName}
                    />
                </label>
                <label>
                    NationalCode:
                    <input 
                        type="text" 
                        name="NationalCode" 
                        onChange={handleChange} 
                        value={data.NationalCode}
                    />
                </label>
                <button className={`submit-btn ${isEdit ? "bgc-edit" : "bgc-submit"}`} type="submit" onClick={handleSubmit}>{isEdit ? "Edit" : "Add"}</button>
            </form>
        </div>
    )
}

export default Form;