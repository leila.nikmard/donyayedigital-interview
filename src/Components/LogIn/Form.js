import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";

function LoginForm ({match , history}){
    const [isRegister , setIsRegister] = useState(false)
    const [formData , setFormData] = useState({
        FirstName:"",
        LastName:"",
        Email:"",
        password:"",
    });

    useEffect(() => {
        const url = match.path.slice(1);
        url === "register" ? setIsRegister(true) : setIsRegister(false)
    },[match.path]);

    const handleChange = (e) => {
        const {value ,name} = e.target;
        setFormData({
            ...formData,
          [name] : value ,
        })
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if(isRegister){
            localStorage.setItem("login" , JSON.stringify(formData));
            history.push("/login")
        }else{
            const getLocalData = JSON.parse(localStorage.getItem("login"));
            if(getLocalData.Email == formData.Email && getLocalData.password == formData.password){
                history.push("/")
            }else{
                alert("please email and password enter true")
            }
        }
        setFormData({
            FirstName:"",
            LastName:"",
            Email:"",
            password:"",
        })
    };

    return(
        <React.Fragment>
            <form>
                {isRegister &&
                    <>
                        <label>
                            FirstName:
                            <input 
                                type="text" 
                                name="FirstName" 
                                onChange={handleChange}
                                value={formData.FirstName}
                            />
                        </label>
                        <label>
                            LastName:
                            <input 
                                type="text" 
                                name="LastName" 
                                onChange={handleChange} 
                                value={formData.LastName}
                            />
                        </label>
                    </>
                }
                <label>
                    Email:
                    <input 
                        type="email" 
                        name="Email" 
                        onChange={handleChange}
                        value={formData.Email}
                    />
                </label>
                <label>
                    password:
                    <input 
                        type="password" 
                        name="password" 
                        onChange={handleChange} 
                        value={formData.password}
                    />
                </label>
                <button type="submit" onClick={handleSubmit}>{isRegister ? "Register" : "Login"}</button>
            </form>
        </React.Fragment>
    )

}

export default withRouter(LoginForm);