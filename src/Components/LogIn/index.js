import React from "react";
import {Link} from "react-router-dom";
import Form from "./Form";
import "./style.css";

function LogIn (){
    return(
        <React.Fragment>
            <div className="title">
                <Link to="/login">Login</Link>
                <Link to="/register">Register</Link>
            </div>
            <Form />
        </React.Fragment>
    )

}

export default LogIn;