import React, { useEffect, useState } from "react";
import {images} from "../../utils/imageSource";
import Form from "./Form";
import "./style.css"

function Profile (){
    const [profileData , setProfileData] = useState();
    const [isDisabled , setIsDisabled] = useState(true);
    const {avatar} = images;

    useEffect(() => {
        const storageData = JSON.parse(localStorage.getItem("login"));
        setProfileData(storageData)
    },[]);

    const handleEdit = (e) => {
        e.preventDefault();
        if(isDisabled){
            setIsDisabled(!isDisabled);
        }else{
            localStorage.setItem("login" , JSON.stringify(profileData));
            setIsDisabled(!isDisabled)
        }
    }

    const handleChange = (e) => {
        const {value ,name} = e.target;
        setProfileData({
            ...profileData,
          [name] : value ,
        })
    };

    return(
        <div className="profile">
            <div className="avatar">
                <img src={avatar}/>
            </div>
            <Form 
                data={profileData}
                isDisabled={isDisabled}
                handleEdit={handleEdit}
                handleChange={handleChange}
            />
        </div>
    )

}

export default Profile;