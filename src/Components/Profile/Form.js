import React from "react";

function Form ({handleChange , handleEdit , data , isDisabled}){

    return(
        <form>
            <label>
                FirstName:
                <input 
                    type="text" 
                    name="FirstName" 
                    onChange={handleChange}
                    value={data && data.FirstName}
                    disabled={isDisabled}
                />
            </label>
            <label>
                LastName:
                <input 
                    type="text" 
                    name="LastName" 
                    onChange={handleChange} 
                    value={data && data.LastName}
                    disabled={isDisabled}
                />
            </label>
            <label>
                Email:
                <input 
                    type="email" 
                    name="Email" 
                    onChange={handleChange}
                    value={data && data.Email}
                    disabled={isDisabled}
                />
            </label>
            <label>
                password:
                <input 
                    type="password" 
                    name="password" 
                    onChange={handleChange} 
                    value={data && data.password}
                    disabled={isDisabled}
                />
            </label>
            <button type="submit" onClick={handleEdit}>{isDisabled ? "Edit" : "Submit"}</button>
        </form>
    )

}

export default Form;