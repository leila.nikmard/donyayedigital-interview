import * as config from "../config";

export const getSearchDataService = (query) => fetch(`${config.API_BASE_URL}/gifs/search?api_key=iAs0eKqOVUk9wzXrHe9blSfVgZPlIZ6U&q=${query}` , {
    method: "GET"
})
