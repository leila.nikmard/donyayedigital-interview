import React from 'react';
import RouterComponent from "./routes";
import DefaultLayout from "./Layouts/index";
import "./App.css";

function App (){
    return(
      <main>
        <DefaultLayout>
          <RouterComponent />
        </DefaultLayout>
      </main>
    )
}

export default App;
