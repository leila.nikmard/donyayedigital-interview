import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Search from "./pages/Search";
import Profile from "./pages/Profile/Profile";
import ToDo from "./pages/ToDo/ToDo";
import UserList from "./pages/UserList";


const RouterComponent = () => {
  return (
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/login" exact component={Login} />
        <Route path="/register" exact component={Login} />
        <Route path="/search" exact component={Search} />
        <Route path="/profile" exact component={Profile} />
        <Route path="/todo" exact component={ToDo} />
        <Route path="/users" exact component={UserList} />
      </Switch>
  );
};
export default RouterComponent;
