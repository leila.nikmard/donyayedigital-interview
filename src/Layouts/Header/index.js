import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import "./style.css"

function Header({ match }) {
  const [token, setToken] = useState();

  useEffect(() => {
    setToken(localStorage.getItem("login"))
  }, [match]);

  return (
    <header>
      <div className="container-login">
        {token
          ?
          `WELCOME ${JSON.parse(token).FirstName}`
          :
          <Link to="/login">
            <span>Login</span>
            <span>/</span>
            <span>Register</span>
          </Link>
        }
      </div>
      <ul className="menu">
        <Link to="/">
          <li>Home</li>
        </Link>
        <Link to="/profile">
          <li>Profile</li>
        </Link>
        <Link to="/search">
          <li>Search</li>
        </Link>
        <Link to="/todo">
          <li>To Do</li>
        </Link>
        <Link to="users">
          <li>Users</li>
        </Link>
      </ul>
    </header>
  );
}

export default withRouter(Header);
