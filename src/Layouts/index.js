import React from "react";
import Footer from "../Layouts/Footer/index";
import Header from "../Layouts/Header/index";
import "./style.css"

function DefaultLayout ({children}){
  
    return (
      <React.Fragment>
          <Header/>
            <div className="container">
              {children}
            </div>
          <Footer />
      </React.Fragment>
    );
}

export default DefaultLayout;
