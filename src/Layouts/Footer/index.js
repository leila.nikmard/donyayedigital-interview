import React from "react";
import "./style.css"

function Footer (){
  return (
      <footer>
          <a
              href="https://tddco.ir/"
              target="_blank"
              rel="noopener noreferrer"
          >
              دنیای دیجیتال
          </a>
      </footer>
  );
}
export default Footer;
